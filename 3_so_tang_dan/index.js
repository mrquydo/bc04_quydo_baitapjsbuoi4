/*
Input: Nhập a=  9, b = 8, c = 7

Steps: 
    - Tìm Số nhỏ nhất và gán cho a
    - Tìm số nhỏ nhì gán cho b

Output: a b c >> 7 8 9 

*/
function sapXepTangDan() {
  //   console.log("yes");
  var a = document.getElementById("number-1").value * 1;
  var b = document.getElementById("number-2").value * 1;
  var c = document.getElementById("number-3").value * 1;

  var soTrungGian = 0; /*Đặt Biến Trung Gian*/

  if (a > b) {
    /*Nếu a > b thì gán giá trị b cho a */
    soTrungGian = a;
    a = b;
    b = soTrungGian;
  }

  if (a > c) {
    /*Nếu a > c thì gán giá trị c cho a*/
    soTrungGian = a;
    a = c;
    c = soTrungGian;
  } /*Sau 2 if này thì a đã là giá trị nhỏ nhất, còn lại so sánh b với c*/

  if (b > c) {
    /*Gán giá trị nhỏ nhì cho b*/
    soTrungGian = b;
    b = c;
    c = soTrungGian;
  }
  document.getElementById(
    "result"
  ).innerText = `Thu Tự Tăng Dần: ${a}, ${b}, ${c}`;
}
