/*
Input: 3 4 5 

Step:
    - 3 % 2
    - 4 % 2 >> số chẵn + 1
    - 5 % 2
 
    - Số lẻ = 3- 1

Output: 
    Số chẵn: 1, số lẻ 2
 */

function countNumber() {
  console.log("yes");

  var numb1 = document.getElementById("number-1").value * 1;
  var numb2 = document.getElementById("number-2").value * 1;
  var numb3 = document.getElementById("number-3").value * 1;

  var countEven = 0;

  var countOdd = 0;

  if (numb1 % 2 == 0) {
    countEven += 1;
  }
  if (numb2 % 2 == 0) {
    countEven += 1;
  }
  if (numb3 % 2 == 0) {
    countEven += 1;
  }

  countOdd = 3 - countEven;

  document.getElementById(
    "result"
  ).innerText = `So chẵn là: ${countEven}, số lẻ là: ${countOdd}`;
}
