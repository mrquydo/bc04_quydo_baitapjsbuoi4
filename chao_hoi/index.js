/*
    Input: Chọn Thành Viên (Bố)

   Step: Lấy Ra text value của thẻ option tương ứng
   
   Output: Chào Bố

*/

function chaoHoi() {
  //   console.log("yes");

  var danhSachThanhVien = document.getElementById("thanh-vien");

  var thanhVien = danhSachThanhVien.value;

  var danhXung =
    danhSachThanhVien.options[danhSachThanhVien.selectedIndex].text;

  var loiChao = document.getElementById("loi-chao");

  if (thanhVien == "") {
    loiChao.innerText = "Chào cả nhà";
  } else {
    loiChao.innerText = "Chào: " + danhXung;
  }
}
